var express = require('express');
var app = express();
var server = require('http').createServer(app);

var io = require('./ws').initialize(server);

server.listen(8000, ()=>{console.log('server started on port 8000')});

module.exports = server;
