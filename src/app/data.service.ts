import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private socket = io('http://localhost:8000');

  constructor() {
    this.socket.on('connection', (socket) => {
      console.log('new connecxion made:', socket.id);
    });
  }

      newMessageReceived() {
        const observable = new Observable<{user: String, message: String}>(observer => {
            this.socket.on('new message', (data) => {
                observer.next(data);
            });
            return () => {this.socket.disconnect(); };
        });

        return observable;
    }
}


